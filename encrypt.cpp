/* See LICENSE file for copyright and license details. */

#include "encrypt.h"

Encrypt::Encrypt(unsigned char *tkey, unsigned char *tiv)
{
	loadkey(tkey);
	loadiv(tiv);
}

Encrypt::~Encrypt()
{
}

int
Encrypt::encrypt(unsigned char *text, unsigned char *cipher)
{
	unsigned char tiv[AES_IV];
	copyiv(tiv);

	mbedtls_aes_context aes;
	mbedtls_aes_setkey_enc(&aes, key, 256);
	mbedtls_aes_crypt_cbc(&aes, MBEDTLS_AES_ENCRYPT, 32, tiv, text, cipher);

	#ifdef DEBUG
	int i;
	printf("[encrypt] encrypt cipher: ");
	for (i=0; i<AES_DATA; i++)
		printf("%x", cipher[i]);
	printf("\n");
	#endif

	return 0;
}

int
Encrypt::decrypt(unsigned char *cipher, unsigned char *text)
{
	#ifdef DEBUG
	int i;
	printf("[encrypt] decrypt cipher: ");
	for (i=0; i<AES_DATA; i++)
		printf("%x", cipher[i]);
	printf("\n");
	#endif

	unsigned char tiv[AES_IV];
	copyiv(tiv);

	mbedtls_aes_context aes;
	mbedtls_aes_setkey_dec(&aes, key, 256);
	mbedtls_aes_crypt_cbc(&aes, MBEDTLS_AES_DECRYPT, 32, tiv, cipher, text);

	return 0;
}

void
Encrypt::copyiv(unsigned char *tiv)
{
	int i;

	for (i=0; i<AES_IV; i++)
		tiv[i] = iv[i];
}

void
Encrypt::loadiv(unsigned char *tiv)
{
	int i;

	for (i=0; i<AES_IV; i++)
		iv[i] = tiv[i];
}

void
Encrypt::loadkey(unsigned char *tkey)
{
	int i;

	for (i=0; i<32; i++)
		key[i] = tkey[i];
}
