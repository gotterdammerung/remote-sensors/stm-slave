#include "dth11.h"

Dth11::Dth11(PinName pin ):
temp(0), hume(0)
{
	sensor = new DigitalInOut(pin);
	sensor->input();
}

Dth11::~Dth11()
{
	delete sensor;
}

int
Dth11::read()
{
	temp = 0;
	hume = 0;

	/* Initialization */
	sensor->output();
	sensor->write(0);
	wait_us(18000);
	sensor->input();

	/* Response */
	wait_us(40);
	if (sensor->read()==1)
		return -1;
	wait_us(85);
	if (sensor->read()==0)
		return -2;
	wait_us(80);
	if (sensor->read()==1)
		return -3;

	/* Read */
	hume = readbyte();
	readbyte();
	temp = readbyte();
	readbyte();
	readbyte();

	return 0;

}

int
Dth11::readbyte()
{

	int byte_t;
	int i;

	byte_t = 0;

	for (i=0; i<8; i++)
	{
		while (sensor->read()==0);
		wait_us(40);
		if (sensor->read()==1)
			byte_t |= (1<<(7-i));
		else
			byte_t &= ~(1<<(7-i));
		while (sensor->read()==1);
	}

	return byte_t;
}

int
Dth11::gettemp()
{
	return temp;
}

int
Dth11::gethume()
{
	return hume;
}
