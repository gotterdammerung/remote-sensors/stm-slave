/* See LICENSE file for copyright and license details. */

#ifndef DTH11__HH
#define DTH11__HH

#include "mbed.h"

class Dth11
{
public:
	Dth11(PinName pin);
	virtual ~Dth11();

	int read();

	int gettemp();
	int gethume();

protected:

	DigitalInOut *sensor;

	int temp, hume;

	int readbyte();
};

#endif
