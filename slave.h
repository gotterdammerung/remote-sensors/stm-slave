/* See LICENSE file for copyright and license details. */

#ifndef SLAVE__HH
#define SLAVE__HH

#include "antenna.h"
#include "encrypt.h"
#include "sensors.h"

#include "config.h"

class Slave
{
public:
	Slave();
	virtual ~Slave();

	int waitmaster();
private:
	char data[TRANSFER_DATA];

	Antenna *antenna;
	Encrypt *encrypt;
	Sensors *sensors;

	unsigned char input[TRANSFER_DATA];
	unsigned char output[TRANSFER_DATA];

	int getdata();
	int senddata();

	void copy(unsigned char *des, char *src);
};

#endif
