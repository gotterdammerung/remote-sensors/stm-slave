/* See LICENSE file for copyright and license details. */

#include "sensors.h"

Sensors::Sensors()
{
	dth = new Dth11(dth_pin);
	temp = 0;
	hume = 0;
}

Sensors::~Sensors()
{
}

int
Sensors::getdata()
{
	int err;

	#ifdef DEBUG
	printf("[sensors] dth11: get data\n");
	#endif

	err = dth->read();
	if (err)
	{
		#ifdef DEBUG
		printf("[sensors] dth11: error in read (%d)\n", err);
		#endif
	}

	temp = dth->gettemp();
	hume = dth->gethume();
	return 0;
}

int
Sensors::gethume()
{
	return hume;
}

int
Sensors::gettemp()
{
	return temp;
}
