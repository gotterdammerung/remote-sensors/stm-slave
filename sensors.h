/* See LICENSE file for copyright and license details. */

#ifndef SENSORS__HH
#define SENSORS__HH

#include "mbed.h"

#include "dth11.h"

#include "config.h"

#define PI 3.14159265

class Sensors
{
public:
	Sensors();
	virtual ~Sensors();

	int getdata();
	int gethume();
	int gettemp();
private:
	Dth11 *dth;

	int temp, hume;
};

#endif
