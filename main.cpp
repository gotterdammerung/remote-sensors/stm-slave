/* See LICENSE file for copyright and license details. */

#include "mbed.h"

#include "slave.h"

#include "config.h"

/* Functions declarations */

/* Declarations */

/* Protocols */
BufferedSerial	pc(USBTX, USBRX);

int
main()
{
	/* Protocols */
	pc.set_baud(serialbaudrate);

	/* Check FPU */
	#if (__FPU_PRESENT == 1) && (__FPU_USED == 1)
	#ifdef DEBUG
	printf("[main] Use FPU function (compiler enables FPU)\n");
	#endif
	#endif

	/* Init */
	Slave *slave;
	slave = new Slave();

	/* Main loop */
	while(1)
	{
		#ifdef DEBUG
		printf("[main] LOOP\n");
		#endif
		slave->waitmaster();
	}

	/* End */
	return 0;
}
