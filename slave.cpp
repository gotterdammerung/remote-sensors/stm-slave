/* See LICENSE file for copyright and license details. */

#include "slave.h"

Slave::Slave()
{
	antenna = new Antenna();
	encrypt = new Encrypt((unsigned char*)aeskey, (unsigned char*)aesiv);
	sensors = new Sensors();
}

Slave::~Slave()
{
}

int
Slave::waitmaster()
{
	/* Wait signal */
	#ifdef DEBUG
	printf("[slave] wait command.\n");
	#endif

	memset(&output[0], 0, sizeof(output));
	memset(&input[0], 0, sizeof(input));

	antenna->enable();
	antenna->waitreceive((char*)input, TRANSFER_DATA);

	/* Check command */
	#ifdef DEBUG
	printf("[slave] receive command.\n");
	#endif

	encrypt->decrypt(input, output);
	if (!strcmp((char*)output, "Request data"))
		if (senddata())
		{
			#ifdef DEBUG
			printf("[slave] error: request data\n");
			#endif
			antenna->disable();
			return -1;
		}

	antenna->disable();
	return 0;
}

int
Slave::getdata()
{
	#ifdef DEBUG
	int i;
	printf("[slave] get data\n");
	#endif

	/* Get data from sensors */
	if(sensors->getdata())
		return -1;

	#ifdef DEBUG
	printf(	"[slave] temp: %d\n"
		"[slave] hume: %d\n",
		sensors->gettemp(),
		sensors->gethume()
		);
	#endif

	/* data pre-process */
	memset(&data[0], 0, sizeof(data) );

	sprintf(data, "D,%04d,%03d,F",
		sensors->gettemp(),
		sensors->gethume()
		);
	#ifdef DEBUG
	printf("[slave] send data: ");
	for (i=0; i<TRANSFER_DATA; i++)
		printf("%c",data[i]);
	printf("\n");
	#endif

	return 0;
}

int
Slave::senddata()
{
	#ifdef DEBUG
	printf("[slave] send data: start\n");
	#endif

	/* Send response: accept */
	memset(&input[0], 0, sizeof(input));
	memset(&output[0], 0, sizeof(output));

	copy(input, (char*)"Accept request data");
	encrypt->encrypt(input, output);
	antenna->send((char*)output, TRANSFER_DATA);

	/* Wait response to accept */
	memset(&input[0], 0, sizeof(input));
	memset(&output[0], 0, sizeof(output));

	if (antenna->receive((char*)input, TRANSFER_DATA))
		return -1;
	encrypt->decrypt(input, output);
	if (strcmp((char*)output, "Wait data"))
		return -1;

	/* Get data */
	if (getdata())
		return -1;

	/* Send Data */
	memset(&output[0], 0, sizeof(output));
	memset(&input[0], 0, sizeof(input));

	copy(input, (char*)data);
	encrypt->encrypt(input, output);
	antenna->send((char*)output, TRANSFER_DATA);

	/* Wait response to receive data */
	memset(&output[0], 0, sizeof(output));
	memset(&input[0], 0, sizeof(input));

	if (antenna->receive((char*)input, TRANSFER_DATA))
		return -1;
	encrypt->decrypt(input, output);
	if (strcmp((char*)output, "Receive data"))
		return -1;

	/* Send end: end */
	memset(&output[0], 0, sizeof(output));
	memset(&input[0], 0, sizeof(input));

	copy(input, (char*)"Close request data");
	encrypt->encrypt(input, output);
	antenna->send((char*)output, TRANSFER_DATA);

	return 0;
}

void
Slave::copy(unsigned char *des, char *src)
{
	int i;

	for (i=0; i<strlen((const char*)src); i++)
		des[i] = src[i];
}
